const expect = require('chai').expect;
const { getTrafficHexColor } = require("../src/traffic-light");
const { changeTrafficColor } = require("../src/main");

describe("Traffic lights", () => {
    it("Test traffic light possibilities", () => {
        expect(true).to.equal(true);
        expect(getTrafficHexColor("GREEN")).to.equal("#00FF00");
        expect(getTrafficHexColor("YELLOW")).to.equal("#FFFF00");
        expect(getTrafficHexColor("RED")).to.equal("#FF0000");
        //expect(getTrafficHexColor("blue")).to.equal("#0000FF");
        expect(getTrafficHexColor("blue")).to.equal(undefined);
    });
})

describe("Sidewalk traffic lights", () => {
    it("Test traffic light change", () => {
        expect(changeTrafficColor("noneOnCrossingRoad")).to.equal("RED");
        expect(changeTrafficColor("YELLOW")).to.equal(undefined);
        expect(changeTrafficColor("userOnCrossingRoad")).to.equal("GREEN");
        expect(changeTrafficColor("carSmashOops")).to.equal(undefined);
    });
})