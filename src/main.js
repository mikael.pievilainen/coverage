// something very complex and nice...
// src/main.js
// "Zebra crossing" lights

const changeTrafficColor = (word) => {
    if (word == "noneOnCrossingRoad") {
        return "RED";
    } else if (word == "YELLOW") {
        return undefined;
    } else if (word == "userOnCrossingRoad") {
        return "GREEN";
    } else {
        return undefined;
    }
};

module.exports = { changeTrafficColor };